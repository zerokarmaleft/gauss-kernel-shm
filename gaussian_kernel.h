#ifndef GAUSSIAN_KERNEL_H
#define GAUSSIAN_KERNEL_H

#define _GNU_SOURCE

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

const int M_ROWS        = 48000;
const int N_COLS        = 156;
const double SIGMA      = 0.05;
const int NUM_PROCESSES = 8;

#define OBJ_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)

#endif
