Edward Cho
Assignment 2: Programming assignment on processes
CS4323
Professor Michel Toulouse
February 20, 2012

=============== Introduction ===============

This program generates the Gaussian kernel for a specified dataset by
equally splitting the workload of computing the partial squared
distance between two vectors across a user-specified number of child
processes.

The program utilizes two shared memory segments, and a local
allocation of memory for the resulting Gaussian kernel. One of the
shared memory segments is for the training data, which all child
processes must access. The other shared memory segment contains all of
the matrices of squared distances computed by each child process as a
single block of memory. In order to share the same memory segment, the
parent process passes an offset to the child process at creation. The
child processes are guaranteed to not write to the same location of
memory simultaneously, hence no semaphore locking mechanism is
required. Once all the partial squared distance matrices are computed,
the parent process combines the results to generate the Gaussian
kernel.

===============  Benchmarks  ===============

The program is executed with only a single process by specifying the
command-line argument "-p" with 1.

$> ./gaussian_kernel -p 1
$>

The following benchmarks were taken on CSX0, which has 16 2.4GHz Intel
Xeon processors, a very fast machine.

$> time ./gaussian_kernel -p 1

real	0m4.789s
user	0m4.766s
sys	0m0.022s

For a run with a single process, we see that the total running time
for the program to finish and the total amount of CPU time are roughly
equal, which is expected. The amount of time spent in system calls is
also relatively low.

$> time ./gaussian_kernel -p 2

real	0m2.590s
user	0m5.114s
sys	0m0.023s

For a run with two processes, we that the total running time for the
program is cut nearly in half, by dividing up the workload to two
CPUs. The total amount of CPU time slightly increased, likely due to
the overhead of creating a child process.  The amount of time spent in
system calls is roughly the same.

$> time ./gaussian_kernel -p 3

real	0m1.893s
user	0m5.206s
sys	0m0.022s

For a run with three processes, we see a proportional improvement. The
performance is nearly a third of the original single process run. The
total amount of CPU time again is slightly increased due to additional
bookkeeping for the second child process.

Bumping up the number of processes for each factor of 156 yields an
interesting trend of diminishing returns. The performance does not
continue to increase linearly as we create a larger number of child
processes. By specifying 78 processes, we reach the operating system's
limit on process creation:

$> time ./gaussian_kernel -p 4
real	0m1.450s
user	0m5.358s
sys	0m0.019s

$> time ./gaussian_kernel -p 6
real	0m0.990s
user	0m5.349s
sys	0m0.022s

$> time ./gaussian_kernel -p 12
real	0m0.781s
user	0m6.973s
sys	0m0.024s

$> time ./gaussian_kernel -p 13
real	0m0.776s
user	0m7.356s
sys	0m0.031s

$> time ./gaussian_kernel -p 26
real	0m0.676s
user	0m7.557s
sys	0m0.030s

$> time ./gaussian_kernel -p 39
real	0m0.672s
user	0m8.100s
sys	0m0.034s

$> time ./gaussian_kernel -p 52
real	0m0.651s
user	0m7.458s
sys	0m0.028s

$> time ./gaussian_kernel -p 78
fork() failed. Exiting...

real	0m0.384s
user	0m0.010s
sys	0m0.020s

===============     Bugs     ===============

* If the program is interrupted, about 30M of allocated shared memory
  segments may not be marked for deletion. Subsequent execution of the
  program will allocate new shared memory segments. I am not sure if
  the system cleans these up automatically on logout. These can be
  manually removed by querying the kernel for the shmids with the
  `ipcs` command. Specifying these shmids to the `ipcrm -m <shmid`
  command will remove them.

===============  Development ===============

One of the unnecessary complexities of programming with System V
shared memory segments is the necessity of creating and keeping track
of keys and identifiers. It is not consistent with the standard UNIX
I/O model of filenames and descriptors.

Additionally, working with 2d arrays in a shared memory segment is
cumbersome and requires error-prone pointer arithmetic. The individual
elements of a 2d array (array of arrays) created on the heap:

float **A = (float **) calloc(NUM_ROWS, sizeof(float *));
for (int i = 0; i < NUM_COLS, i++)
    A[i] = (float *) calloc(NUM_COLS, sizeof(float *));

is accessible with straightforward index access (A[i][j]). Storing
pointers in a shared memory segment is problematic since each child
process has its own virtual memory address space, so a pointer in one
process very likely points to some data that is meaningless in another
process.

Most operating systems set strict limitations on maximum process count
compared to threads. Processing a large dataset with a very fine
granularity of parallelism may be impossible due to these process
count limits. This is evident in the sample run above with 78
processes.
