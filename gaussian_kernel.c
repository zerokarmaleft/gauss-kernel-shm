#include "gaussian_kernel.h"

/*
 * The square of x
 */
float
square(float x)
{
    return x * x;
}

/*
 * The Euclidean distance between vectors v1 and v2
 */
float
distance(float *v1, float *v2, int length)
{
    double result = 0.0;
    int i;

    for (i = 0; i < length; i++)
        result += square(v1[i] - v2[i]);

     return sqrt(result);
}

/*
 * Generate random numbers to fill the matrix A
 */
void
fill(float *A, int m, int n)
{
    int i, j = 0;

    srand(time(0));
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            A[i * n + j] = (float) rand()/(float)RAND_MAX;
}

/*
 * Fill matrix A with ones for testing
 */
void
fill_ones(float *A, int m, int n)
{
    int i, j = 0;

    for (i = 0; i < m; i++)
        for(j = 0; j < n; j++)
            A[i * n + j] = 1.0;
}

/*
 * Display a small matrix A for testing
 */
void
print_matrix(float *A, int m, int n)
{
    int i, j = 0;

    for (i = 0; i < m; i++)
        for(j = 0; j < n; j++) {
            if (j == 0)
                printf("[ %4.2lf", A[i * n + j]);
            else if (j == n - 1)
                printf(" %4.2lf ]\n", A[i * n + j]);
            else
                printf(" %4.2lf", A[i * n + j]);
        }
    printf("\n");
}

/*
 * Compute the Gaussian kernel serially
 */
void
gaussian_kernel(float *X, float *K, int m, int n)
{
    float v1[n], v2[n];
    int i, j, k = 0;

    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                v1[k] = X[i * n + k];
                v2[k] = X[j * n + k];
            }
            K[i * n + j] = exp(-square(distance(v1, v2, 3)) / (2 * square(SIGMA)));
        }
}

/*
 * Normalize a Gaussian kernel
 */
void
normalize_gaussian_kernel(float *K, int m)
{
    int i, j;
    double sum = 0.0;

    for (i = 0; i < m; i++)
        for (j = 0; j < m; j++)
            sum += K[i * m + j];

    for (i = 0; i < m; i++)
        for (j = 0; j < m; j++)
            K[i * m + j] = K[i * m + j] / sum;
}

void
parallel_gaussian_kernel(int k, int shmid_X, int shmid_N, int m, int r)
{
    float *X, *N;

    /* attach shared memory segments */
    X = (float *) shmat(shmid_X, NULL, 0);
    if (X == (void *) -1) {
        fprintf(stderr, "shmat() for training data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    N = (float *) shmat(shmid_N, NULL, 0);
    if (N == (void *) -1) {
        fprintf(stderr, "shmat() for intermediate data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }

    /* compute squared distance between two vectors */
    float sum;
    for (int i = 0; i < m; i++)
        for (int j = 0; j < m; j++) {
            sum = 0.0;

            for (int d = 0; d < r; d++) {
                sum += square(X[i + d] - X[j + d]);
            }

            N[(i * m) + (k * m * m) + j] = sum;
        }

    /* detach shared memory segments */
    if (shmdt(X) == -1) {
        fprintf(stderr, "shmdt() for training data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    if (shmdt(N) == -1) {
        fprintf(stderr, "shmdt() for intermediate data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
}

int
main(int argc, char *argv[])
{
    /* parse command-line options */
    int opt;
    int num_processes = NUM_PROCESSES;
    while ((opt = getopt(argc, argv, "p:")) != -1) {
        switch (opt) {
        case 'p':
            num_processes = atoi(optarg);
            if ((N_COLS % num_processes) != 0) {
                fprintf(stderr, "Invalid argument: Work units cannot be divided evenly amongst %d processes\n",
                    num_processes);
                exit(EXIT_FAILURE);
            }
            break;
        default:
            abort();
        }
    }
    
    float *X, *N, **K;
    int shmid_X, shmid_N;
    int status;
    pid_t worker_pid, worker_pids[num_processes];

    /* allocate memory for training data and intermediate data */
    shmid_X = shmget(IPC_PRIVATE,
                   M_ROWS * N_COLS * sizeof(float),
                   IPC_CREAT | OBJ_PERMS);
    if (shmid_X == -1) {
        fprintf(stderr, "Size of shared memory segment: %ld\n", (long) (M_ROWS * N_COLS * sizeof(float)));
        fprintf(stderr, "shmget() for training data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    shmid_N = shmget(IPC_PRIVATE,
                     num_processes * N_COLS * N_COLS * sizeof(float),
                     IPC_CREAT | OBJ_PERMS);
    if (shmid_N == -1) {
        fprintf(stderr, "Size of shared memory segment: %ld\n", (long) (num_processes * M_ROWS * N_COLS * sizeof(double)));
        fprintf(stderr, "shmget() for intermediate data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }

    /* attach shared memory segments */
    X = (float *) shmat(shmid_X, NULL, 0);
    if (X == (void *) -1) {
        fprintf(stderr, "shmat() for training data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    N = (float *) shmat(shmid_N, NULL, 0);
    if (N == (void *) -1) {
        fprintf(stderr, "shmat() for intermediate data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }

    /* allocate memory for Gaussian kernel */
    K = (float **) calloc(N_COLS, sizeof(float *));
    for (int i = 0; i < N_COLS; i++)
        K[i] = (float *) calloc(N_COLS, sizeof(float *));
            
    /* initialize training data matrix */
    fill(X, M_ROWS, N_COLS);

    /* partition work units across child processes */
    for (int k = 0; k < num_processes; k++) {
        switch (worker_pids[k] = fork()) {
        case -1:
            fprintf(stderr, "fork() failed. Exiting...\n");
            exit(EXIT_FAILURE);
        case 0:
            parallel_gaussian_kernel(k, shmid_X, shmid_N, N_COLS, M_ROWS / num_processes);
            _exit(EXIT_SUCCESS);
        default:
            break;
        }
    }

    /* wait for child processes to complete */
    for (;;) {
        if ((worker_pid = wait(&status)) == -1) {
            if (errno == ECHILD) {
                /* no more child processes left */
                break;
            } else {
                fprintf(stderr, "wait() failed. Exiting...\n");
                exit(EXIT_FAILURE);
            }
        }
    }

    /* combine results of child processes to final result */
    float sum;
    for (int i = 0; i < N_COLS; i++)
        for (int j = 0; j < N_COLS; j++) {
            sum = 0.0;

            for (int k = 0; k < num_processes; k++)
                sum += N[(i * N_COLS) + (k * N_COLS * N_COLS) + j] / (2 * square(SIGMA));

            K[i][j] = exp(-sum);
        }

    /* detach shared memory segments */
    if (shmdt(X) == -1) {
        fprintf(stderr, "shmdt() for training data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    if (shmdt(N) == -1) {
        fprintf(stderr, "shmdt() for intermediate data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }

    /* mark shared memory segment for deletion */
    if (shmctl(shmid_X, IPC_RMID, NULL) == -1) {
        fprintf(stderr, "shmctl(IPC_RMID) for training data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    if (shmctl(shmid_N, IPC_RMID, NULL) == -1) {
        fprintf(stderr, "shmctl(IPC_RMID) for intermediate data failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}
